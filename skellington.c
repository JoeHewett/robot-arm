#include "dynamixel.h"
#include <stdio.h>
#include <time.h>
#include <unistd.h>

void move_to_location(int connection, unsigned char id, unsigned char loc_h, unsigned char loc_l) {

	unsigned char cs = ~ ( id + 0x07 + 0x03 + 0x1e + loc_l + loc_h +
				0x30 + 0x00);

	unsigned char arr[] = { 0xff, 0xff, id, 0x07, 0x03, 0x1e, loc_l, loc_h, 0x30, 0x00, cs };

	int buff_len = 100;
	unsigned char buff[buff_len];


	int bytes_read = write_to_connection(connection,arr,11,buff,buff_len);
	
	printf("Move_to_location x called\n");
}

void wait_until_done(int connection, unsigned char id) {
	usleep(2000000);
}

int changePosition(int connection, int height, int rotation, int grip, int pickingup) {
	int motor1;	
	int motor2;
	int motor3;
	int motor4;
	int motor5; 

	switch (height) {
		case 1:

			motor2 = 0x30;
			motor3 = 0x35;
			motor4 = 0x90;
			break;
		case 2:
			motor2 = 0x40;
			motor3 = 0x40;
			motor4 = 0x70;
			break;
		case 3:
			motor2 = 0x40;
			motor3 = 0x70;
			motor4 = 0x30;

			break;
		case 4:
			motor2 = 0x70;
			motor3 = 0x80;
			motor4 = 0x20;
	
			break;
	}

	switch (rotation) {
		case 1:
			motor1 = 0x20;
			break;
		case 2:
			motor1 = 0x80;
			break;
		case 3:
			motor1 = 0xE0;
			break;
	}

	if (grip == 1)
		motor5 = 0x00;
	else 
		motor5 = 0xff;


	if (pickingup == 1) { 
		move_to_location(connection,1,0x01,motor1);
		wait_until_done(connection, 1);
	}

	move_to_location(connection,2,0x01,motor2);
	
	move_to_location(connection,3,0x01,motor3);

	move_to_location(connection,4,0x01,motor4);
 
	wait_until_done(connection, 4);

	move_to_location(connection,5,0x01,motor5);
	wait_until_done(connection,5);

	
	if (pickingup == 0) { 
		move_to_location(connection,1,0x01,motor1);
		wait_until_done(connection, 1);
	}

		
	return 0;
}



int main(int argc, char* argv[]) {

	int connection = open_connection("/dev/ttyUSB0",B1000000);
	
	// Reset back to 4 middle
	changePosition(connection, 4, 2, 0, 0);

	changePosition(connection, 3, 1, 1, 1);
	changePosition(connection, 4, 2, 1, 0);

	changePosition(connection, 1, 3, 0, 1);
	changePosition(connection, 4, 2, 0, 0);

	changePosition(connection, 2, 1, 1, 1);
	changePosition(connection, 4, 2, 1, 0);

	changePosition(connection, 1, 2, 0, 1);
	changePosition(connection, 4, 2, 0, 0);

	changePosition(connection, 1, 3, 1, 1);
	changePosition(connection, 4, 2, 1, 0);

	changePosition(connection, 2, 2, 0, 1);	
	changePosition(connection, 4, 2, 0, 0);

	changePosition(connection, 1, 1, 1, 1);
	changePosition(connection, 4, 2, 1, 0);

	changePosition(connection, 1, 3, 0, 1);
	changePosition(connection, 4, 2, 0, 0);

	changePosition(connection, 2, 2, 1, 1);
	changePosition(connection, 4, 2, 1, 0);

	changePosition(connection, 1, 1, 0, 1);
	changePosition(connection, 4, 2, 0, 0);

	changePosition(connection, 1, 2, 1, 1);
	changePosition(connection, 4, 2, 1, 0);

	changePosition(connection, 2, 3, 0, 1);
	changePosition(connection, 4, 2, 0, 0);

	changePosition(connection, 1, 1, 1, 1);
	changePosition(connection, 4, 2, 1, 0);

	changePosition(connection, 3, 3, 0, 1);
	changePosition(connection, 4, 2, 0, 0);

	changePosition(connection, 4, 2, 1, 1);
				
	return 0;

}
